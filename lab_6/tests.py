from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from datetime import date
import unittest
from .models import Status
from .forms import StatusForm
from .views import index, challenge
from selenium import webdriver
import time
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options

# Create your tests here.

class Lab1UnitTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.browser = webdriver.Chrome("./chromedriver",chrome_options=chrome_options)


    def test_lab_6_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_else_does_not_exist(self):
        response = Client().get('/antah-berantah/')
        self.assertEqual(response.status_code,404)

    def test_status_create_is_working(self):
        Status.objects.create(status="Test status berhasil")
        amount_of_status = Status.objects.all().count()
        self.assertEqual(amount_of_status, 1)

    def test_is_data_valid(self):
        form = StatusForm({
            "status" : "status aku mantul"
        })
        self.assertTrue(form.is_valid())
        self.instance = form.save()
        self.assertEqual(self.instance.status, "status aku mantul")

    def test_lab_6_using_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'lab1.html')


    def test_if_comment_is_inserted(self):
        self.browser.get('http://localhost:8000')
        comment_box = self.browser.find_element_by_name("status")
        comment_box.send_keys('Coba Coba')
        comment_box.submit()
        self.assertIn("Coba Coba", self.browser.page_source)

    def test_if_challenge_is_clicked_redirect_to_challenge_page(self):
        self.browser.get('http://localhost:8000')
        challenge_link = self.browser.find_element_by_css_selector("#challenge")
        challenge_link.click()
        self.assertIn("Ahmad Irfan", self.browser.page_source)

    # Challenge Tes CSS Property

    def test_if_greetings_uses_the_right_property(self):
        self.browser.get('http://localhost:8000')
        elemen1 = self.browser.find_element_by_css_selector(".greetings")
        CSSValue = elemen1.value_of_css_property("position")
        self.assertTrue(CSSValue == "absolute")
        CSSValue2 = elemen1.value_of_css_property("padding")
        self.assertTrue(CSSValue2 == "5px")

    def test_if_paragraph_uses_the_right_property(self):
        self.browser.get('http://localhost:8000')
        elemen1 = self.browser.find_element_by_css_selector("p")
        CSSValue = elemen1.value_of_css_property("font-family")
        print(CSSValue)
        self.assertTrue(CSSValue == "Overpass, sans-serif")

    # Challenge Tes Layout

    def test_if_button_is_displayed(self):
        self.browser.get('http://localhost:8000')
        elemen1 = self.browser.find_element_by_css_selector(".statusform")
        self.assertTrue(elemen1.is_displayed)

    def test_if_form_is_displayed(self):
        self.browser.get('http://localhost:8000')
        elemen1 = self.browser.find_element_by_css_selector(".btn")
        self.assertTrue(elemen1.is_displayed)

    def tearDown(self):
        self.browser.quit()


class ChallengeUnitTest(TestCase):

    def test_challenge_is_exist(self):
        response = Client().get('/challenge/')
        self.assertEqual(response.status_code, 200)

    def test_challenge_using_template(self):
        response = Client().get('/challenge/')
        self.assertTemplateUsed(response, 'challenge.html')

    def test_challenge_using_challenge_function(self):
        found = resolve('/challenge/')
        self.assertEqual(found.func, challenge)

    def test_nama_in_page(self):
        new_response = Client().get('/challenge/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Ahmad Irfan', html_response)

    def test_NPM_in_page(self):
        new_response = Client().get('/challenge/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('1806235864', html_response)

    