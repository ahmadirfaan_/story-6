from django.shortcuts import render, redirect
from .forms import StatusForm
from .models import Status
import os


def index(request):
	status_form = StatusForm(request.POST or None)
	if request.method == 'POST' and status_form.is_valid():
		status_form.save()
	return render(request, 'lab1.html', 
		{
		"form":StatusForm(),
		"status":Status.objects.all(),
		})

def challenge(request):
	return render(request, 'challenge.html')