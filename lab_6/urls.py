from django.urls import re_path
from .views import index, challenge
#url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path(r'^challenge', challenge, name='challenge')
]
